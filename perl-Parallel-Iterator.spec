%global _empty_manifest_terminate_build 0
Name:           perl-Parallel-Iterator
Version:        1.002
Release:        1
Summary:        Simple parallel execution
License:        GPL-1.0-only or Artistic-1.0-Perl
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Parallel-Iterator/
Source0:        https://www.cpan.org/modules/by-module/Parallel/Parallel-Iterator-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  perl-generators
BuildRequires:  perl(Config)
BuildRequires:  perl(IO::Handle)
BuildRequires:  perl(IO::Select)
BuildRequires:  perl(Module::Build)
BuildRequires:  perl(Storable)
BuildRequires:  perl(Test::More)
Requires:       perl(Config)
Requires:       perl(IO::Handle)
Requires:       perl(IO::Select)
Requires:       perl(Storable)
%description
The map function applies a user supplied transformation function to
each element in a list, returning a new list containing the
transformed elements.
%package help
Summary : Simple parallel execution
Provides: perl-Parallel-Iterator-doc
%description help
The map function applies a user supplied transformation function to
each element in a list, returning a new list containing the
transformed elements.
%prep
%setup -q -n Parallel-Iterator-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -delete
%{_fixperms} -c %{buildroot}

%{_fixperms} $RPM_BUILD_ROOT/*

pushd %{buildroot}
touch filelist.lst
if [ -d usr/bin ];then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ];then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ];then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib ];then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .
%check || :
make test

%clean
rm -rf $RPM_BUILD_ROOT

%files -f filelist.lst
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/*
%files help
%{_mandir}/*

%changelog
* Mon Feb 05 2024 woody2918 <wudi1@uniontech.com> 1.002-1
- update version to 1.002
- Packaging fix to stop installing boilerplate.pl
- Documentation fixes
- Packaging improvements

* Sun May 23 2021 Perl_Bot <Perl_Bot@openeuler.org> 1.00-1
- Specfile autogenerated by Perl_Bot
